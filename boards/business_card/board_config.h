#ifndef BOARD_CONFIG_H
#define BOARD_CONFIG_H

#define CRYSTALLESS    1

#define VENDOR_NAME "0x220E"
#define PRODUCT_NAME "business_card"
#define VOLUME_LABEL "BUSINESSBOOT"
#define INDEX_URL "http://0x220E.ch"
#define BOARD_ID "buisness_card-python-V001"

//#define USB_VID 0x239A
//#define USB_PID 0x001E

//#define LED_PIN PIN_PA10
//#define LED_TX_PIN PIN_PA27
//#define LED_RX_PIN PIN_PB03

//#define BOARD_RGBLED_CLOCK_PIN            PIN_PA01
//#define BOARD_RGBLED_DATA_PIN             PIN_PA00

#endif
